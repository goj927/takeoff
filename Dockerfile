FROM ruby:2.4-alpine

COPY Gemfile .
COPY Gemfile.lock .

WORKDIR app
ADD . /app/

RUN apk add --update build-base git openssh jq curl
RUN gem install bundler --no-ri --no-rdoc
RUN bundle config --local path vendor/bundle
RUN bundle install --path vendor/bundle --retry 3 --deployment --without development
